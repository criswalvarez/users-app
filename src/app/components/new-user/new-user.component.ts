import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  @Input('user') user : User
  @Output('onRegister') onRegister = new EventEmitter <User>();
  
  constructor(private auth : AuthService, private router : Router) { }

  ngOnInit(){}

  NewUser(user : User){
    this.auth.register();
  }
 
  goToMain(){
    this.router.navigate(['/main'])
  }
}


