import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  Users = new Array<User>();

  constructor() { }

  register(name: string, surname : string, telephone : number, image : string) {
    let user = new User();
    user.image = image;
    user.name = name;
    user.surname = surname;
    user.telephone = telephone;
    this.Users.push(user);
  }
  
}
