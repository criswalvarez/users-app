import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { MainComponent } from './components/main/main.component';
import { NewUserComponent } from './components/new-user/new-user.component';




const routes: Routes = [
  { path : 'login', component: UserDetailComponent},
  { path : 'new-user', component: NewUserComponent},
  { path : '**' , component : NewUserComponent},
  { path : 'main' , component : MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
